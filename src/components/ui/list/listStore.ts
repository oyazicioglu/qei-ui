import { writable, Writable } from 'svelte/store'

export interface ListITem {
    id: string
    active: boolean
    text: string
    value: any
}

export const listStore: Writable<ListITem[]> = writable([])
export const activeListItem: Writable<ListITem> = writable(undefined)

export const addListItem = (item: ListITem) => {
    listStore.update((current) => {
        return [...current, item]
    })
}

export const removeListItem = (item: ListITem) => {
    listStore.update((current) => {
        const otherItems = current.filter((i) => i !== item)
        return otherItems
    })
}
