import { writable, Writable } from 'svelte/store'

export interface SelectItem {
    id: string
    active: boolean
    text: string
    value: any
}

export const listStore: Writable<SelectItem[]> = writable([])
export const activeSelectItem: Writable<SelectItem> = writable(undefined)

export const addSelectItem = (item: SelectItem) => {
    listStore.update((current) => {
        return [...current, item]
    })
}

export const removeSelectItem = (item: SelectItem) => {
    listStore.update((current) => {
        const otherItems = current.filter((i) => i !== item)
        return otherItems
    })
}
